module Data.Sudoku.Grid where

import           Data.Vector (Vector)
import qualified Data.Vector as Vector

data Grid a = Grid {-# UNPACK #-} !Int {-# UNPACK #-} !Int !(Vector a)
  deriving (Eq, Ord, Show, Functor, Foldable, Traversable)

newGrid :: Int -> a -> Grid a
newGrid cell el = Grid cell size (Vector.replicate (size * size) el)
  where size = cell * cell

gridIx :: Int -> Int -> Grid a -> Maybe a
gridIx x y (Grid _ size elems) = elems V.!? (x + y * size)
